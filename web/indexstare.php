<?php
require_once __DIR__.'/../vendor/autoload.php'; //ładuje klasy kiedy dana klasa jest potrzebna


$app = new Silex\Application();   //utwórz nowy obiekt o nazwie app klasy appliaction nalezacy do przestrzeni nazw Silex
$app['debug'] = true;


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));


$app->get('/hello/{name}', function ($name) use ($app) {
    return $app['twig']->render('hello.twig', array(
        'name' => $name,
    ));
});



$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello '.$app->escape($name);
});



$data = array(
    0 => array(
        'name' => 'John',
        'email' => 'john@example.com',
    ),
    1 => array(
        'name' => 'Mark',
        'email' => 'mark@example.com',
    ),
);

$app->get('/data', function () use ($data) {
    $view = '';
    foreach ($data as $row) {
        $view .= $row['name'];
        $view .= ' : ';
        $view .= $row['email'];
        $view .= '<br />';
    }
    return $view;
});



$app->get('/data/{id}', function (Silex\Application $app, $id) use ($data) {
    if (!isset($data[$id])) {
        $app->abort('404', 'Invalid entry');
    } else {
        $view = '';
        $view .= $data[$id]['name'];
        $view .= ' : ';
        $view .= $data[$id]['email'];
    }
    return $view;
});





$app->run();
