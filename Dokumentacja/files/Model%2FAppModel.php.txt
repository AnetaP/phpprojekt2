<?php
/**
 * Created by PhpStorm.
 * User: Aneta
 * Date: 08.05.14
 * Time: 18:21
 */

namespace Model;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;

class AppModel
{

    protected $_db;

    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Get's the list of vocabulary ID's
     *
     * @access public
     * @return array 
     */
    public function getID()
    {
        $sql = 'SELECT idVocabulary FROM Vocabulary';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Get's random vocabulary ID
     *
     * @access public
     * @return array 
     */
    public function getRandomID()
    {
        $sql = 'SELECT idVocabulary FROM `Vocabulary` WHERE idVocabulary >= (
            SELECT FLOOR( MAX(idVocabulary) * RAND()) FROM `Vocabulary` ) 
            ORDER BY idVocabulary LIMIT 1;';
        return $this->_db->fetchAssoc($sql);
    }

    /**
     * Get's random word
     *
     * @access public
     * @return array 
     */
    public function getRandomWord($condition)
    {

         $sql = "SELECT * FROM `Vocabulary` WHERE 
         idVocabulary NOT IN (".$condition.") AND idUsers = 1 OR idUsers = 2 ORDER BY RAND() LIMIT 1;";
 
        return $this->_db->fetchAssoc($sql);
    }

    /**
     * Get's random word from a certain category
     *
     * @access public
     * @return array 
     */
    public function getRandomWordCat($condition, $id)
    {

     $sql = "SELECT * FROM `Vocabulary` WHERE 
     idVocabulary NOT IN (".$condition.") AND idCategories = (".$id.")  
     ORDER BY RAND() LIMIT 1;";
 
        return $this->_db->fetchAssoc($sql);
    }

     /**
     * Ads final result of the test
     *
     * @access public
     * @return array 
     */
    public function addFinalResult($right, $wrong, $idUsers)
    {

        $sql = "INSERT INTO `answers` (`right`, `wrong`, `idUsers`) 
        VALUES (?,?,?);";
        $this->_db->executeQuery($sql, array($right, $wrong, $idUsers));
    }

     /**
     * Gets results of a certain user
     *
     * @access public
     * @return array 
     */
    public function getResults($idUsers)
    {
        $sql = "SELECT * FROM `answers` WHERE 
        idUsers = (".$idUsers.") ORDER BY DATE DESC;";
        return $this->_db->fetchAll($sql);
    }

    
    public function getResultsPage($page, $limit, $pagesCount){
    {
    if (($page <= 1) || ($page > $pagesCount)) {
        $page = 1;
    }
       $sql = 'SELECT * FROM `answers` WHERE 
        idUsers = (".$idUsers.") ORDER BY DATE DESC LIMIT :start, :limit';
    //$sql = 'SELECT * FROM posts ORDER BY date DESC LIMIT :start, :limit';
    $statement = $this->_db->prepare($sql);
    $statement->bindValue('start', ($page-1)*$limit, \PDO::PARAM_INT);
    $statement->bindValue('limit', $limit, \PDO::PARAM_INT);
    $statement->execute();
    return $statement->fetchAll();
    }
    }






}






