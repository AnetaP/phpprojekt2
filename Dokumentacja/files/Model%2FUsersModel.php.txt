<?php

namespace Model;

use Doctrine\DBAL\DBALException;
use Silex\Application;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class usersModel
{

    protected $_app;
    protected $_db;

    public function __construct(Application $app)
    {
        $this->_app = $app;
        $this->_db = $app['db'];
    }

     /**
     * Load user by his login
     *
     * @access public
     * @return array 
     */
    public function loadUserByLogin($login)
    {
        $data = $this->getUserByLogin($login);

        if (!$data) {
            throw new UsernameNotFoundException(
                sprintf(
                    'Username "%s" does not exist.', $login
                )
            );
        }

        $roles = $this->getUserRoles($data['id']);

        if (!$roles) {
            throw new UsernameNotFoundException(
                sprintf(
                    'Username "%s" does not exist.', $login
                )
            );
        }

        $user = array(
            'login' => $data['login'],
            'password' => $data['password'],
            'roles' => $roles
        );

        return $user;
    }

     /**
     * Get user by his login
     *
     * @access public
     * @return array 
     */
    public function getUserByLogin($login)
    {
        $sql = 'SELECT * FROM users WHERE login = ?';
        return $this->_db->fetchAssoc($sql, array((string) $login));
    }

    /**
     *Get user by his ID
     *
     * @access public
     * @return array 
     */
    public function getUserById($id)
    {
        $sql = 'SELECT * FROM users WHERE id = ?';
        return $this->_db->fetchAssoc($sql, array((string) $id));
    }

    /**
     * Get user's role
     *
     * @access public
     * @return array 
     */
    public function getUserRoles($userId)
    {
        $sql = '
            SELECT
                roles.role
            FROM
                users_roles
            INNER JOIN
                roles
            ON users_roles.role_id=roles.id
            WHERE
                users_roles.user_id = ?
            ';

        $result = $this->_db->fetchAll($sql, array((string) $userId));

        $roles = array();
        foreach ($result as $row) {
            $roles[] = $row['role'];
        }

        return $roles;
    }

    /**
     * Get all users
     *
     * @access public
     * @return array 
     */
    public function getAll()
    {
        $sql = 'SELECT * FROM users';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Save user
     *
     * @access public
     * @return array 
     */
    public function saveUser($data)
    {
       
        $sql = 'UPDATE users SET email = ?, password = ? WHERE id = ?';
        $this->_db->executeQuery(
            $sql, array(
            $data['email'], $data['new_password'], $data['id'])
        );
   
    }

    /**
     * Register new user
     *
     * @access public
     * @return array 
     */
    public function Register($data)
    {
        $sql = 'INSERT INTO users (login, password, email) VALUES (?,?,?)';
        $this->_db->executeQuery(
            $sql, array(
            $data['login'], $data['password'], $data['email'])
        );
    }  

    /**
     * Add role to user
     *
     * @access public
     * @return array 
     */
    public function AddUserRole($id)
    {
        $sql = 'INSERT INTO users_roles (user_id, role_id) VALUES (?,?)';
        $this->_db->executeQuery($sql, array($id, 2));

    }

    /**
     * Delete user and all his data
     *
     * @access public
     * @return array 
     */
    public function deleteUser($data)
    {

        $sql = 'DELETE FROM users WHERE id = ?;
                DELETE FROM users_roles WHERE user_id = ?;
                DELETE FROM Categories WHERE idUser = ? ';
 
        $this->_db->executeQuery(
            $sql, array(
            $data['id'], $data['id'], $data['id'])
        );

    }

    /**
     * Check if ID exists
     *
     * @access public
     * @return array 
     */
    public function idExist($idUser)
    {
        if (($idUser != '') && ctype_digit((string)$idUser)) {
            $sql = 'SELECT * FROM users WHERE id= ?';
            if ($this->_db->executeUpdate($sql, array((int) $idUser)) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

