<?php

namespace Controller; //informacja ze bedzie to kontroler

use Silex\Application;   //zalaczenie dodatkowych bibliotek
use Silex\ControllerProviderInterface;   //
use Symfony\Component\HttpFoundation\Request; //
use Symfony\Component\Validator\Constraints as Assert;
use Model\AlbumsModel;

class AlbumsController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $albumsController = $app['controllers_factory'];
        $albumsController->get('/', array($this, 'index'))->bind('/albums/');
        $albumsController->match('/add', array($this, 'add'))->bind('/albums/add');
        $albumsController->match('/edit/{id}', array($this, 'edit'))->bind('/albums/edit');
        $albumsController->match('/delete/{id}', array($this, 'delete'))->bind('/albums/delete');
        $albumsController->get('/view/{id}', array($this, 'view'))->bind('/albums/view');
        return $albumsController;
    }

    public function index(Application $app)
    {
        $albumsModel = new AlbumsModel($app);
        $albums = $albumsModel->getAll();
        return $app['twig']->render('index.twig', array('albums' => $albums));
    }

    public function add(Application $app, Request $request)
    {
        // default values:
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('artist', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $albumsModel = new AlbumsModel($app);
            $albumsModel->addAlbum($form->getData());
            return $app->redirect($app['url_generator']->generate('/albums/'), 301);
        }

        return $app['twig']->render('index.twig', array('form' => $form->createView()));
    }

    public function edit(Application $app, Request $request)
    {
        $albumsModel = new AlbumsModel($app);

        $id = (int) $request->get('id', 0);

        $album = $albumsModel->getAlbum($id);

        if (count($album)) {

            $form = $app['form.factory']->createBuilder('form', $album)
                ->add('id', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                ))
                ->add('title', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('artist', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('save', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $albumsModel = new AlbumsModel($app);
                $albumsModel->saveAlbum($form->getData());
                return $app->redirect($app['url_generator']->generate('/albums/'), 301);
            }

            return $app['twig']->render('albums/edit.twig', array('form' => $form->createView(), 'album' => $album));

        } else {
            return $app->redirect($app['url_generator']->generate('/albums/add'), 301);
        }

    }

    public function delete(Application $app)
    {
        return 'Delete Action';
    }

    public function view(Application $app)
    {
        return 'View Action';
    }

}
