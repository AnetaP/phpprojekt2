<?php
namespace Controller;
use Model\categoryModel;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class categoryController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $categoryController = $app['controllers_factory'];
        $categoryController->get('/', array($this, 'index'))->bind('/categories/');
         $categoryController->match('/public', array($this, 'publicCategories'))->bind('/categories/public/');
        $categoryController->match('/add', array($this, 'add'))->bind('/categories/add/');
        $categoryController->match('/edit/{idCategories}', array($this, 'edit'))->bind('/categories/edit/');
        $categoryController->match('/userCategories/{idUsers}', array($this, 'userCategories'))->bind('/categories/userCategories/');
        $categoryController->match('/delete/{idCategories}', array($this, 'delete'))->bind('/categories/delete/');
        $categoryController->get('/view', array($this, 'view'))->bind('/categories/view/');
        return  $categoryController;
    }
 
    /**
     * Categories list
     *
     * Displays the list of all categories
     *
     * @access public
     * @return array Categories
     */
    public function index (Application $app, Request $request)
    {


        $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getPublic();
        return $app['twig']->render('/categories/all.twig', array('Categories' => $categories));
    
    }

    /**
     * Personal user's categories list
     *
     * Displays the list of categories of the user
     *
     * @access public
     * @return array Categories
     */
    public function userCategories (Application $app, Request $request)
    {
        $usersController = new UsersController($app); 
        $idUsers = (int) $request->get('idUsers');
        $usersController = new UsersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);

        var_dump($idUsers);
        $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getUserCategories($idUsers);
        return $app['twig']->render('/categories/index.twig', array('Categories' => $categories, 'idUsers' => $idUsers, 'current' => $currentUser));
    }

    /**
     * Public Categories
     * 
     * Displays the list of public categories
     *
     * @access public
     * @return array Categories
     */
    public function publicCategories (Application $app, Request $request)
    {
       $categoryModel = new categoryModel($app);
        $categories = $categoryModel->getPublic();
        return $app['twig']->render('/categories/index.twig', array('Categories' => $categories));
    }

    /**
     * Add Category
     * 
     * @access public
     * @return array 
     */
    public function add(Application $app, Request $request)
    {
        $usersController = new UsersController($app); 
        $idUsers = $usersController-> getIdCurrentUser($app);
         $typeCategories = 0;

    
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)

            ->add(
                'nameCategories', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
                )
            )
            ->add('Dodaj', 'submit')
            ->getForm();

        $form->handleRequest($request);


        if ($form->isValid()) {
            $categoryModel = new categoryModel($app);
            $categoryModel->addCategory($form->getData(), $typeCategories, $idUsers);
            return $app->redirect($app['url_generator']->generate('/categories/'), 301);
        }

        return $app['twig']->render('categories/add.twig', array('form' => $form->createView()));
    }

    /**
     * Edit category
     * 
     * @access public
     * @return array 
     */
    public function edit(Application $app, Request $request)
    {
        $categoryModel = new categoryModel($app);
        $idCategories = (int) $request->get('idCategories', 0);
        $category = $categoryModel->getCategory($idCategories);
        $usersController = new UsersController($app); 
        $idUsers = (int) $request->get('idUsers');
        $usersController = new UsersController($app); 
        $currentUser = (int) $usersController-> getIdCurrentUser($app);
        $catUser= (int) $category['idUser'];
         
    if ($currentUser == $catUser) {

        if (count($category)) {

            $form = $app['form.factory']->createBuilder('form', $category)
                ->add(
                    'idCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'nameCategories', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
                    )
                )
                ->add('save', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $categoryModel = new categoryModel($app);
                $categoryModel->saveCategory($form->getData());
                return $app->redirect($app['url_generator']->generate('/categories/'), 301);
            }

            return $app['twig']->render('categories/edit.twig', array('form' => $form->createView(), 'category' => $category, 'idUsers' => $idUsers, 'current' => $currentUser));

        } else {

           return $app->redirect($app['url_generator']->generate('/categories/add/'), 301);
        }
    } else {

        return $app['twig']->render('auth/error.twig');
    }

    }

     /**
     * Delete Category
     * 
     * @access public
     * @return array 
     */
     public function delete(Application $app, Request $request)
     {
        $categoryModel = new categoryModel($app);

        $idCategories = (int) $request->get('idCategories');

        if (ctype_digit((string)$idCategories) && $categoryModel->idExist($idCategories)) {
            $category = $categoryModel->getCategory($idCategories);
        } else {
            return $app->redirect($app['url_generator']->generate('/categories/'), 301);
        }

        if (count($category)) {

            $form = $app['form.factory']->createBuilder('form', $category)
                ->add(
                    'idCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank())
                    )
                )
                ->add(
                    'nameCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
                    )
                )
                ->add(
                    'typeCategories', 'hidden', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 1)))
                    )
                )
                ->add('delete', 'submit')
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $categoryModel = new categoryModel($app);
                $categoryModel->deleteCategory($form->getData());
                $app['session']->getFlashBag()->add('message', 'Usunięto kategorie!');
                return $app->redirect($app['url_generator']->generate('/categories/'), 301);
            }

            return $app['twig']->render('categories/delete.twig', array('form' => $form->createView(), 'Category' => $category));

        } else {
            return $app->redirect($app['url_generator']->generate('/categories/'), 301);
        }
     }

}

